#ifndef H_CONST_DEFINES
#define H_CONST_DEFINES

const uint8_t RECEIVING = 0;
const uint8_t PROCESSING = 1;
const uint8_t ACTION = 2;

const uint8_t FALSE = 0;
const uint8_t TRUE = 0;

#endif // H_CONST_DEFINES
