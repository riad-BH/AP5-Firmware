#if !defined(H_CONFIG)
#define H_CONFIG
#include "IncludeFiles.h"
#include "IOFunctions.h"

// Function to dispay information in the begining
void sendStartingScreen();

// Function to set up inputs and outputs
void setIO();
#endif // H_CONFIG
