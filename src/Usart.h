#if !defined(H_USART)
#define H_USART
#include "ArduinoInclude.h"

// Function for starting the USART
void setUSART0(const uint32_t);
void setUSART1(const uint16_t);
void unsetUSART1();

#endif // H_USART
