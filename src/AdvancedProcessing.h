#ifndef H_ADVANCEDPROCESSING
#define H_ADVANCEDPROCESSING

#include <Arduino.h>
#include "Setting.h"
#include "ConstDefines.h"
#include "Sending.h"
#include "Mouvement.h"
#include "MouvementQueue.h"
#include "Receiving.h"

void advancedProcessing(MouvementQueue &m_q);

#endif // END H_ADVANCEDPROCESSING
