#include "IOFunctions.h"

	/////////////////////// A ///////////////////////
	IOPin D_PIN_22 = IOPin(PA0, CODE_PORTA);
	IOPin D_PIN_23 = IOPin(PA1, CODE_PORTA);
	IOPin D_PIN_24 = IOPin(PA2, CODE_PORTA);
	IOPin D_PIN_25 = IOPin(PA3, CODE_PORTA);
	IOPin D_PIN_26 = IOPin(PA4, CODE_PORTA);
	IOPin D_PIN_27 = IOPin(PA5, CODE_PORTA);
	IOPin D_PIN_28 = IOPin(PA6, CODE_PORTA);
	IOPin D_PIN_29 = IOPin(PA7, CODE_PORTA);
	/////////////////////// B ///////////////////////
	IOPin D_PIN_53 = IOPin(PB0, CODE_PORTB);
	IOPin D_PIN_52 = IOPin(PB1, CODE_PORTB);
	IOPin D_PIN_51 = IOPin(PB2, CODE_PORTB);
	IOPin D_PIN_50 = IOPin(PB3, CODE_PORTB);
	IOPin D_PIN_10 = IOPin(PB4, CODE_PORTB);
	IOPin D_PIN_11 = IOPin(PB5, CODE_PORTB);
	IOPin D_PIN_12 = IOPin(PB6, CODE_PORTB);
	IOPin D_PIN_13 = IOPin(PB7, CODE_PORTB);
	/////////////////////// C ///////////////////////
	IOPin D_PIN_37 = IOPin(PC0, CODE_PORTC);
	IOPin D_PIN_36 = IOPin(PC1, CODE_PORTC);
	IOPin D_PIN_35 = IOPin(PC2, CODE_PORTC);
	IOPin D_PIN_34 = IOPin(PC3, CODE_PORTC);
	IOPin D_PIN_33 = IOPin(PC4, CODE_PORTC);
	IOPin D_PIN_32 = IOPin(PC5, CODE_PORTC);
	IOPin D_PIN_31 = IOPin(PC6, CODE_PORTC);
	IOPin D_PIN_30 = IOPin(PC7, CODE_PORTC);
	/////////////////////// D //////////////////////
	IOPin D_PIN_21 = IOPin(PD0, CODE_PORTD);
	IOPin D_PIN_20 = IOPin(PD1, CODE_PORTD);
	IOPin D_PIN_19 = IOPin(PD2, CODE_PORTD);
	IOPin D_PIN_18 = IOPin(PD3, CODE_PORTD);
	IOPin D_PIN_38 = IOPin(PD7, CODE_PORTD);
	/////////////////////// E /////////////////////////
	IOPin D_PIN_0 = IOPin(PE0, CODE_PORTE);
	IOPin D_PIN_1 = IOPin(PE1, CODE_PORTE);
	IOPin D_PIN_5 = IOPin(PE3, CODE_PORTE);
	IOPin D_PIN_2 = IOPin(PE4, CODE_PORTE);
	IOPin D_PIN_3 = IOPin(PE5, CODE_PORTE);
	/////////////////////// F //////////////////////
	IOPin A_PIN_1 = IOPin(PF0, CODE_PORTF);
	IOPin A_PIN_2 = IOPin(PF1, CODE_PORTF);
	IOPin A_PIN_3 = IOPin(PF2, CODE_PORTF);
	IOPin A_PIN_4 = IOPin(PF3, CODE_PORTF);
	IOPin A_PIN_5 = IOPin(PF4, CODE_PORTF);
	IOPin A_PIN_6 = IOPin(PF5, CODE_PORTF);
	IOPin A_PIN_7 = IOPin(PF6, CODE_PORTF);
	/////////////////////// G ////////////////////////
	IOPin D_PIN_41 = IOPin(PG0, CODE_PORTG);
	IOPin D_PIN_40 = IOPin(PG1, CODE_PORTG);
	IOPin D_PIN_39 = IOPin(PG2, CODE_PORTG);
	IOPin D_PIN_4 = IOPin(PG5, CODE_PORTG);
	/////////////////////// H ////////////////////////
	IOPin D_PIN_17 = IOPin(PH0, CODE_PORTH);
	IOPin D_PIN_16 = IOPin(PH1, CODE_PORTH);
	IOPin D_PIN_6 = IOPin(PH3, CODE_PORTH);
	IOPin D_PIN_7 = IOPin(PH4, CODE_PORTH);
	IOPin D_PIN_8 = IOPin(PH5, CODE_PORTH);
	IOPin D_PIN_9 = IOPin(PH6, CODE_PORTH);
	/////////////////////// J ///////////////////////
	IOPin D_PIN_15 = IOPin(PJ0, CODE_PORTJ);
	IOPin D_PIN_14 = IOPin(PJ1, CODE_PORTJ);
	/////////////////////// K //////////////////////
	IOPin A_PIN_8 = IOPin(PK0, CODE_PORTK);
	IOPin A_PIN_9 = IOPin(PK1, CODE_PORTK);
	IOPin A_PIN_10 = IOPin(PK2, CODE_PORTK);
	IOPin A_PIN_11 = IOPin(PK3, CODE_PORTK);
	IOPin A_PIN_12 = IOPin(PK4, CODE_PORTK);
	IOPin A_PIN_13 = IOPin(PK5, CODE_PORTK);
	IOPin A_PIN_14 = IOPin(PK6, CODE_PORTK);
	IOPin A_PIN_15 = IOPin(PK7, CODE_PORTK);
	/////////////////////// L //////////////////////
	IOPin D_PIN_49 = IOPin(PL0, CODE_PORTL);
	IOPin D_PIN_48 = IOPin(PL1, CODE_PORTL);
	IOPin D_PIN_47 = IOPin(PL2, CODE_PORTL);
	IOPin D_PIN_46 = IOPin(PL3, CODE_PORTL);
	IOPin D_PIN_45 = IOPin(PL4, CODE_PORTL);
	IOPin D_PIN_44 = IOPin(PL5, CODE_PORTL);
	IOPin D_PIN_43 = IOPin(PL6, CODE_PORTL);
	IOPin D_PIN_42 = IOPin(PL7, CODE_PORTL);