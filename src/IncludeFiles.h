#ifndef H_INCLUDE_FILES
#define H_INCLUDE_FILES

#include "Arduino.h"
#include "Setting.h"
#include "Pins.h"
#include "PinFunctions.h"
#include "ConstDefines.h"
#include "TimerSetting.h"
#include "ADC.h"
#include "ExternalInterrupts.h"
#include "LimitSwitch.h"
#include "Usart.h"
#include "Receiving.h"
#include "Sending.h"
#include "TempControl.h"
#include "Processing.h"
#include "AdvancedProcessing.h"
#include "Motors.h"



#endif // H_INCLUDE_FILES